package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello () {
		return "Hello World!";
	}

	@GetMapping("/hi")
	public String hi (@RequestParam(value = "name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	@GetMapping("hello/{name}")
	public String courses (@PathVariable ("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}


	//ACTIVITY

	ArrayList enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam String user) {
		enrollees.add(user);
		return "Thank you for enrolling, " + user + "!";
	}

	@GetMapping("/getEnrollees")
	public ArrayList getEnrollees(){
		return enrollees;
	}

	@GetMapping("/nameage")
	public String nameAge(@RequestParam String name, @RequestParam int age) {
		return "Hello " + name + "! My age is " + age + ".";
	}

	@GetMapping("/courses/{id}")
	public String courseId(@PathVariable ("id") String id) {
		String message;
		if(id.equals("java101")){
			message = "Name: Java101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
		}
		else if(id.equals("sql101")){
			message = "Name: SQL101, Schedule: MWF 12:00 PM - 4:00 PM, Price: PHP 3100";
		}
		else if(id.equals("javaee101")){
			message = "Name: JavaEE101, Schedule: TTH 8:00 AM - 11:00 AM, Price: PHP 3000";
		}
		else{
			message = "Course cannot be found!";
		}

		return message;
	}

}
